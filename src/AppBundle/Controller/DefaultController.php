<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;
use AppBundle\Service\Helper;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request, LoggerInterface $logger)
    {
        //$logger = $this->container->get('monolog.logger');
        $logger->info('Hit on homepage');

        $helperService = $this->container->get(Helper::class);

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'random_string' => $helperService->randomString()
        ]);
    }

    /**
     * @Route("/home", name="home")
     */
    public function homeAction(Request $request)
    {
        $name = $request->get('name');
        $lastName = $request->get('lastName');

        $error = null;
        if(!$name || !$lastName){
            $error = 'Prénom ou Nom manquant !';
        }

        // replace this example code with whatever you need
        return $this->render('default/home.html.twig', [
            'name' => $name,
            'lastName' => $lastName,
            'error' => $error
        ]);
    }

    /**
     * @Route("/home/{name}/{lastName}", 
     *  name="homeSlug", 
     *  requirements={"name":"^[a-zA-Z]+$","lastName":"^[a-zA-Z]+$"})
     */
    public function homeslugAction($name = 'Prenom', $lastName/* = 'Nom'*/, Request $request)
    {
        return $this->render('default/home.html.twig', [
            'name' => $name,
            'lastName' => $lastName,
            'error' => null
        ]);
    }

    /**
     * @Route("/url", name="url")
     */
    public function urlAction(){
        $url = $this->generateUrl('home');
        $urlSlug = $this->generateUrl('homeSlug',
            [
                'name'=>'unprenom',
                'lastName'=>'unnom'
            ]
        );
        $urlAbsolute = $this->generateUrl('url',[],UrlGeneratorInterface::ABSOLUTE_URL);
        return $this->render('default/url.html.twig',[
            'homeUrl' => $url,
            'homeSlug' => $urlSlug,
            'urlAbsolute' => $urlAbsolute
        ]);
    }

    /**
     * @Route("/redirect", name="redirect")
     */
    public function redirectAction(){
        return $this->redirectToRoute('homepage',[]);
        //return $this->redirect('https://www.google.com');
    }

    /**
     * @Route("/not-found", name="not-found")
     */
    public function notFoundAction(){
        throw $this->createNotFoundException('Erreur 404 - Non trouvé !');
    }

    /**
     * @Route("/crash", name="crash")
     */
    public function crashAction(){
        try {
            throw new \Exception('Crash !!!');
        } catch(\Exception $e){
            return new Response($e->getMessage());
        }
    }
}
