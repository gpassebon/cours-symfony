<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\BlogPost;
use AppBundle\Form\BlogPostType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\CommentType;

/**
 * @Route("/blog")
 */
class BlogPostController extends Controller
{
    /**
     * @Route("/", name="blog_home")
     */
    public function listAction()
    {
        $blogPostRepo = $this->getDoctrine()->getRepository(BlogPost::class);

        $blogPosts = $blogPostRepo->getPublishedBlogPost();

        return $this->render('AppBundle:BlogPost:list.html.twig', array(
            'blog_posts' => $blogPosts
        ));
    }

    /**
     * @Route("/post/{id}", name="blog_post")
     */
    public function viewAction(BlogPost $blogPost, SessionInterface $sessionService, Request $request)
    {
        if(!$blogPost->getPublished()){
            throw $this->createNotFoundException('Post non trouvé');
        }

        $form = $this->createForm(CommentType::class);
        $form->add('save',SubmitType::class, [
            'label' => 'Enregistrer']);
        $form->handleRequest($request);      
        
        if($form->isSubmitted() && $form->isValid()){
            $comment = $form->getData();
            $comment->setPost($blogPost);
            $comment->setCreatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->persist($comment);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Commentaire publié');
            
            $form = $this->createForm(CommentType::class);
            $form->add('save',SubmitType::class, [
                'label' => 'Enregistrer']);
        }

        $lastVisitedPosts = array();
        if($sessionService->has('lastVisitedPosts'))
        {
            $lastVisitedPosts = $sessionService->get('lastVisitedPosts');
        }
        array_push($lastVisitedPosts,array($blogPost->getTitle(),new \DateTime()));
        $sessionService->set('lastVisitedPosts',$lastVisitedPosts);

        return $this->render('AppBundle:BlogPost:view.html.twig', array(
            'blog_post' => $blogPost,
            'comment_form' => $form->createView()
        ));
    }

    /**
     * @Route("/new", name="blog_new")
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(BlogPostType::class);
        $form->add('save',SubmitType::class, [
            'label' => 'Enregistrer']);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $blogPost = $form->getData();
            $this->getDoctrine()->getManager()->persist($blogPost);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('blog_post', ['id' => $blogPost->getId()]);
        }
        return $this->render('AppBundle:BlogPost:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/edit/{id}", name="blog_edit")
     * @Security("has_role('ROLE_USER')")
     */
    public function editAction(BlogPost $blogPost, Request $request)
    {
        /*
        if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            throw $this->createAccessDeniedException();
        }
        */

        $form = $this->createForm(BlogPostType::class,$blogPost);

        $form->add('save',SubmitType::class, [
            'label' => 'Mettre à jour']);
        
        $form->handleRequest($request);

        //$this->addFlash('success', 'Success');
        //$this->addFlash('warning', 'Warning');

        if($form->isSubmitted() && $form->isValid()){
            $blogPost = $form->getData();
            //$blogPost->setUpdatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->persist($blogPost);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','
            Votre post '.$blogPost->getTitle().' a été mis à jour.');

            return $this->redirectToRoute('blog_post', ['id' => $blogPost->getId()]);
        }
        
        $formDelete = $this->createDeleteForm($blogPost);

        return $this->render('AppBundle:BlogPost:edit.html.twig', array(
            'blog_post' => $blogPost,
            'form' => $form->createView(),
            'form_delete' => $formDelete->createView()
        ));
    }

    /**
     * @Route("/delete/{id}", name="blog_delete")
     * @Method("DELETE")
     */
    public function deleteAction(BlogPost $blogPost, Request $request)
    {
        if(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            throw $this->createAccessDeniedException();
        }

        $form = $this->createDeleteForm($blogPost);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->getDoctrine()->getManager()->remove($blogPost);
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success','Post '.$blogPost->getTitle().' supprimé.');
            
            return $this->redirectToRoute('blog_home');
        }else{
            throw $this->createAccessDeniedException('Forbiden delete');
        }
    }

    /**
     * Creates a form to delete a blogPost entity.
     *
     * @param BlogPost $blogPost The blogPost entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BlogPost $blogPost)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('blog_delete', array('id' => $blogPost->getId())))
            ->setMethod('DELETE')
            ->add('delete_blog',SubmitType::class,array(
                'label' => 'Supprimer'
            ))
            ->getForm()
        ;
    }

}
