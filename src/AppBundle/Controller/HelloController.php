<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HelloController extends Controller
{
    /**
     * @Route("/hello/index")
     */
    public function indexAction()
    {
        return $this->render('@App/Hello/index.html.twig', array(
            // ...
        ));
    }
}
