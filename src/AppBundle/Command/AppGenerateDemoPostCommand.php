<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\BlogPost;

class AppGenerateDemoPostCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:generate:demo-post')
            ->setDescription('Add a demo BlogPost')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {  
        $blogPost = new BlogPost();
        $blogPost->setTitle('Demo Title');
        $blogPost->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed condimentum, odio et varius egestas, tortor nulla pretium velit, in faucibus metus odio id ante. Duis vitae neque et dui feugiat egestas a a ipsum. Sed non massa vitae nulla pharetra molestie eget et purus. Nam eget bibendum enim, at blandit nunc. Nunc porta porttitor est, laoreet laoreet velit facilisis at. Curabitur quis facilisis felis, commodo pharetra nisi. Duis a urna diam. Phasellus eu sapien sit amet sem mollis dictum. Cras ultrices, lorem ut varius tincidunt, dolor risus ullamcorper erat, sit amet lobortis lectus leo et urna.

        Curabitur ultricies sed diam pellentesque congue. Quisque vulputate dictum felis, sollicitudin tincidunt sapien bibendum at. Integer id ligula erat. Integer in rhoncus diam. Nam id pulvinar turpis. Sed volutpat auctor velit, sit amet mollis sapien lobortis eu. Ut cursus semper velit nec convallis. Sed dictum leo non felis ornare sodales. Vestibulum varius tristique sem, nec eleifend mi congue nec. Curabitur efficitur in tortor et fermentum. Suspendisse potenti.');
        $blogPost->setAuthor('Demo User');
        $blogPost->setCreateAt(new \DateTime());
        $blogPost->setUpdatedAt(new \DateTime());
        $blogPost->setPublished(true);

        $entityManager = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $entityManager->persist($blogPost);
        $entityManager->flush();
        
        $output->writeln('BlogPost persisted ID : '.$blogPost->getId());
    }

}
