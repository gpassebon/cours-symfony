<?php
namespace AppBundle\Service;

class Helper {
    
    public function randomString($length = 8){
        return substr(str_shuffle(str_repeat('abcdefgahijklmnopqrstuvwxyz0123456789',10)),0,$length);
    }

}